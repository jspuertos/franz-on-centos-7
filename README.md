# Franz on CentOS Linux 7

Franz Messenger is a Cross-Platform application which allows you to access your favorite messaging apps including WhatsApp, Facebook Messenger, Slack, HipChat, etc. In this project, we are going to install franz on CentOS Linux  7

## System Requirements
Assume that you already have CentOS Linux 7 and the EPEL repository.
- Download franz for linux from the [official page](https://meetfranz.com/) (Ubuntu)

## Installation
### Install EPEL packages
Install the alien and rpmrebuild packages from the EPEL repository
```python
yum install -enablerepo=epel alien
yum install -enablerepo=epel rpmrebuild
```

### Convert package from .deb to .rpm format
To convert .deb file to a .rpm package, run:
```python
sudo alien -r /path-of-file/franz_5.0.0-beta.18_amd64.deb
```
Is necessary delete one dependency and a path of the .rmp package created with alien:
```python
sudo rpmrebuild -pe /path-of-file/franz-5.0.0_beta.18-904.x86_64.rpm
```
That will be open up a text editor.
In that text editor delete the lines:
```python
Requires:      libffmpeg.so()(64bit)
.........
.........
%dir %attr(0775, root, root) "/"

```
When prompted: 
```python
Do you want to continue?

```
Enter "y" to rebuild the package. Pay attention to the result because the new .npm package will be created in a different location

### Intall the package
Run:
```python
sudo yum install /path-of-file/franz-5.0.0_beta.18-904.x86_64.rpm

```
## References
- <https://wolfecomputer.wordpress.com/2017/07/14/how-to-install-a-deb-package-on-centos-linux-7/>
- <https://www.thegeekstuff.com/2010/11/alien-command-examples/?utm_source=feedburner&utm_medium=email&utm_campaign=Feed%253A+TheGeekStuff+(The+Geek+Stuff)>